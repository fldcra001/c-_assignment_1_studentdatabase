/*
 * StudentRecord.h
 * 
 * Created on 2 March 2014
 * Craig Feldman
 */
 
#ifndef STUDENT_RECORD_H
#define STUDENT_RECORD_H

#include <string>
#include <iostream>
#include "counter.h"
#include "tokenlib/tokenlib.h"

 
namespace fldcra001 {
	
 class StudentRecord  : public  sjp::counter<StudentRecord> //inherit
 {
	 // ------------------------------------------------
	private:
		std::string name_;
		std::string surname_;
		std::string studentNumber_;
		std::string classRecord_;
		int token_;
	//--------------------------------------------------
		
	public:
		
		//constructor
		StudentRecord(std::string name, std::string surname, std::string studentNumber, std::string classRecord) 
						: name_(name), surname_(surname), studentNumber_(studentNumber), classRecord_(classRecord), token_(tokenlib::acquire_token()) {};
						
		//copy from rhs
		StudentRecord(const StudentRecord & rhs):
			name_(rhs.name_), surname_(rhs.surname_), studentNumber_(rhs.studentNumber_), classRecord_(rhs.classRecord_) {};
			
		//copy assignment
		StudentRecord & operator=(const StudentRecord & rhs);
						
		//move from rhs
		StudentRecord(StudentRecord && rhs): 
			name_(std::move(rhs.name_)), surname_(std::move(rhs.surname_)), studentNumber_(std::move(rhs.studentNumber_)), classRecord_(std::move(rhs.classRecord_))  {};
						
		//move assignment operator
		StudentRecord & operator=(StudentRecord && rhs);
		
		//destructor
		~StudentRecord();			
		
		//overload stream operators
		friend std::ostream& operator<<(std::ostream& os, const StudentRecord& sr);
		
		friend std::istream& operator>>(std::istream& is, StudentRecord& sr);
		
		std::string getName(void);
		std::string getStudentNumber(void);
		double getAverage(void);
	
		
		
 };		 
		 
} //namespace

#endif
