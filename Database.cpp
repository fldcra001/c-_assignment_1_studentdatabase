/*
 * Database.cpp
 * 
 * Class to manage the database
 * 
 * Created on 8 March 2014
 * Craig Feldman
 */
 
 #include "Database.h"
 #include "StudentRecord.h" //
 #include <iostream>
 #include <sstream>
 #include <iomanip>
 #include <iterator>
 #include <fstream>
 
 #include <memory>
 
 namespace fldcra001 {
	 	 
	/** adds student to database */
	void Database::addStudent(void) {
		using namespace std;
		
		//iterate to ensure student doesnt exist
		string name, sur, num, rec;
		cout << "Please enter the student details in the the following form:\n[name] [surname] [student number] [class record]\n" << std::endl;
		//StudentRecord r;
		//cin >> r;
		cin >> name;
		cin >> sur;
		cin >> num;
		getline(cin, rec);
		
		//iterate to ensure student doesnt exist
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		while (it != records.end()) {
			if ((*it)->getStudentNumber() == num) {
				std::cout << "\nStudent already exists and will be overwritten." << std::endl;
				records.erase(it);
				break;
			}
			it++;
		}	
		
		std::cout <<  "Student Created\n" << std::endl;
		
		
		std::unique_ptr<StudentRecord> s(new StudentRecord(name, sur, num, rec));
		//StudentRecord::print_counts(std::cout,"StudentRecord");		
		records.push_back (std::move(s));		
		//StudentRecord::print_counts(std::cout,"StudentRecord");		
		
	 }
	 
	 
	/** deletes student from database who matches given student num */	
	void Database::remove(std::string studentNum) {
		
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		
		while (it != records.end()) {
			if ((*it)->getStudentNumber() == studentNum) {
				records.erase(it);
				std::cout << "\nStudent removed:\n" << std::endl;
				return;
			}
			it++;
		}	
		std::cout <<  "\nError: Student not found.\n" << std::endl;	
	}
	
	/** reads in entries from specified file*/
	void Database::read(std::string file) {
		using namespace std;
		
		ifstream tf(file);
		string line; 
		
		string name, sur, num, rec;
		
		if (!tf) {
			cerr << "File open failed. Check that file exists \n" << endl;
			return;
		}
			
		while ( std::getline( tf, line )) {
			istringstream iss(line);
			iss >> name;
			iss >> sur;
			iss >> num;
			getline(iss, rec); //remaining is student record
			
			//iterate to ensure student doesnt exist
			std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
			while (it != records.end()) {
			if ((*it)->getStudentNumber() == num) {
				std::cout << "\nStudent already exists and will be overwritten." << std::endl;
				records.erase(it);
				break;
			}
			it++;
		}	
		
			unique_ptr<StudentRecord> s(new StudentRecord(name, sur, num, rec));
			//StudentRecord::print_counts(std::cout,"StudentRecord");		
			records.push_back (move(s));
		}	
		
		cout << "\nDone\n" << endl;
			
	}
	 
	/** iterates through list and saves to specified file*/
	void Database::save(std::string file){
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		std::ofstream tf(file, std::ios::app); //will append to file
		std::string line; 		
				
		if (!tf) {
			std::cerr << "File write failed. Check that file exists \n" << std::endl;
			return;
		}
		while (it != records.end()) {
			tf << **it;
			it++;
		}	
		std::cout <<  "\nDatabase saved.\n" << std::endl;
	}
	 
	/** displays student data where matching student num */	
	void Database::display(std::string studentNum){
		
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		while (it != records.end()) {
			if ((*it)->getStudentNumber() == studentNum) {
				std::cout << "\nStudent Found:\n" << std::endl;
				printHeader();
				std::cout << **it;
				return;
			}
			it++;
		}	
		std::cout <<  "\nError: Student not found.\n" << std::endl;
	}
	
	
	/** displays marks and average of student matching student num */		 
	void Database::grade(std::string studentNum){
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		
		while (it != records.end()) {
			if ((*it)->getStudentNumber() == studentNum) {
				std::cout << "\nStudent Found:\n" << std::endl;
				printHeader();
				std::cout << **it;
				std::cout << "\nThis student achieved an average of: " << (*it)->getAverage() << std::endl; 
				return;
			}
			it++;
		}	
		std::cout << "\nError: Student not found." << std::endl;
	}
	
		 
		 
	/** displays all the student data*/	
	void Database::displayAll(){
		std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		printHeader();
		
		while (it != records.end()) {
			std::cout << **it;
			it++;
		}	
	}
	
	/** returns student number of top student*/	
	std::string Database::findTop(){
		 std::list<std::unique_ptr<StudentRecord>>::iterator it = records.begin();
		 float currAvg, maxAvg;
		 std::string max, studentNum;
		 maxAvg = (*it)->getAverage(); //initially max is first element
		 studentNum = (*it)->getStudentNumber();
		 
		while (it != records.end()) {
			currAvg = (*it)->getAverage();
			std::cout << "curr avg " << currAvg << "maxAvg " << maxAvg <<std::endl;
			if (currAvg > maxAvg) 
				studentNum = (*it)->getStudentNumber(); //new max
							
			it++;
		}
		
		std::cout << "\nHere are the details for the top student:" << std::endl;
		//printHeader();
		return studentNum;
	}
		 
	void Database::close(){
		records.clear();
	}	
		
		
	/** prints column headings */	
	void Database::printHeader() {
		using std::cout;
		using std::setw;
		cout << "------------------------------------------------------------------------------" << std::endl;
		cout << setw(0) << "Name" <<
				setw(20) << "Surname" <<
				setw(25) << "Student Number" <<
				setw(20) << "Class Record" << std::endl;
		cout << "------------------------------------------------------------------------------" << std::endl;
	}

		 
 } //namespace
