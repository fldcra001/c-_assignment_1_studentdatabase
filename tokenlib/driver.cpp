#include "tokenlib.h"

int main(void)
{
	int token1 = tokenlib::acquire_token();
	tokenlib::check_tokens();

	int token2 = tokenlib::acquire_token();
	tokenlib::check_tokens();

	int token3 = tokenlib::acquire_token();
	tokenlib::check_tokens();

	tokenlib::release_token(token1);
	tokenlib::release_token(token2);
	tokenlib::release_token(token3);

	tokenlib::check_tokens();

	// This should throw an exception
	//tokenlib::release_token(1000000);

	tokenlib::final_token_check();

	// Make it fail
	int last_token = tokenlib::acquire_token();
	tokenlib::final_token_check();
}