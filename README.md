**The original assignment requirements and instructions can be found in the 'Downloads' section.**

*********************
Author: Craig Feldman

Date: 9 March 2014
*********************

=======================
Assignment 1 - c++ database
Readme.txt
========================


----------------
  Instructions
----------------

 1. Compile the tokenlib files by navigating to the tokenlib folder and running the 'make' command (Do this before step 2).

 2. Compile the program with the makefile provided in the root directory using the 'make' command.

 3. Execute the program by typing './myprog -f [file]' where [file] is a specified database file (.txt). If file is left out, a default database, 'default.txt', is used.

 4. Select one of the options by typing in the number (or letter) that corresponds to the option you want to perform.

        1 - Add a student in the form as specified by the program.
        2 - Delete a student by entering a student number.
        3 - Read a database from a file you specify, or simply type 'd' to use 'default.txt' or the file specified at runtime by you.
        4 - Save to the file specified at runtime (or default.txt if no file specified)
        6 - Enter a student number to view their record and average grade.
        7 - Display all the students that are loaded.
        8 - Find student with highest average mark.
        q - Exit.

 5. Run 'make clean' to remove object files.


--------------------------
  List of provided files
--------------------------

root folder:

* cmdline_parser.cpp Database.h 
* StudentRecord.h
* cmdline_parser.h 
* default.txt 
* Readme.txt         
* counter.h 
* driver.cpp    
* Database.cpp 
* Makefile 
* StudentRecord.cpp

tokenlib folder:
	
* driver.cpp 
* Makefile
* tokenlib.cpp
* tokenlib.h


--------
  Info
--------

 I have submitted my zipped up git repo, in case there is a problem with this, I have zipped up the source files in the zip called "backup".

 I used Ubuntu on a virtual machine for this project. All code has been tested and compiles and runs as expected.
 'Geany' was used as the IDE.
 
 The makefile generates an executable called 'myprog'.

 The following is a list of the cpp files I have created:

* StudentRecord.cpp - Stores student record information
* Database.cpp - models the database and performs all database functions
* driver.cpp - contains main() and runs program
* default.txt - contains some default student record data for reading (if you want).

 Please see the files for further details.

 This program is case-sensitive.

 When you quit the program using the 'q' command, the necessary token and constructor/destructor information will be displayed.
 In all my tests, the final token check was successful.
		  
-------------------
  Troubleshooting
-------------------

 Ensure the all files exist in the same directory (except for the tokenlib files which should be in a folder).

 This program uses c++ 11 features. So please ensure you have the latest version/compiler.

 

** Enjoy!**