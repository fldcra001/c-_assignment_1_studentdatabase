/* driver for Assignment 1
 * provides the text based interface
 * 
 * Craig Feldman
 * 2 March 2014
 */


#include "StudentRecord.h"
#include "counter.h"
#include "Database.h"
#include "tokenlib/tokenlib.h"
#include "cmdline_parser.h"

#include <iostream>
#include <fstream>



int main (int argc, char* argv[]) {

	using namespace fldcra001;
	using namespace std;
		
	//cmd line parser
	//-----------------------------------------------------------------------------
	cmdline_parser parser;

	if(!parser.process_cmdline(argc, argv))
	{
		std::cerr << "Couldn't process command line arguments" << std::endl;
		return 1;
	}

	if(parser.should_print_help())
		{ parser.print_help(std::cout);	}

	string file_name = parser.get_filename();
	cout << "-----------------------------------------" << endl;
	
	//input output file stream with specified file name
	fstream file(file_name.c_str());
	//file.open();
	if(!file)
    {
        cout << "'" << file_name << "' does not exist. New database file will be created" << endl;
        file.open(file_name.c_str(), fstream::out);
    }
    else //exists
		cout << "Database file '" << file_name << "' has been opened" << endl;
	cout << "-----------------------------------------\n" << endl;

	//file << "test" <<endl;
	file.close();
	//------------------------------------------------------------------------------
	
	//new database
	Database db;
	
			//StudentRecord s("a", "a", "a", "a");

			//StudentRecord::print_counts(std::cout,"StudentRecord");

	string option, input;
	
	for (;;) { //loop forever
		cout << "1: Add student" <<endl;
		cout << "2: Delete student" <<endl;
		cout << "3: Read database" <<endl;
		cout << "4: Save database" <<endl;
		cout << "5: Display given student data" <<endl;
		cout << "6: Grade student" <<endl;
		cout << "7: Dispay all student data" <<endl;
		cout << "8: Find winning student" <<endl;
		cout << "q: Quit" <<endl;
		cout << "\nEnter an option (or q to quit) and press return." <<endl;
		//StudentRecord s("a", "a", "a", "a");
		//StudentRecord::print_counts(std::cout,"StudentRecord");
		cin >> option;
		cout << endl;
		
		if (option == "1")
			db.addStudent(); 
			
		else if (option == "2"){
			cout << "Please enter the student number of the student that you wish to delete:" << endl;
			cin >> input;
			db.remove(input);
		}
		else if (option == "3") {
			cout << "Please enter the file name to read from\nEnter 'd' for default or file specified at runtime." << endl;
			cin >> input;
			if (input == "d")
				db.read(file_name);
			else
				db.read(input);
		}
		else if (option == "4") {
			cout << "Saving to " << file_name << endl;
			db.save(file_name);
		}
		else if (option == "5") {
			cout << "Please enter the student number of the student that you wish to locate:" << endl;
			cin >> input;
			db.display(input);
			cout << endl;

		}
		else if (option == "6") {
			cout << "Please enter the student number of the student that you wish to grade:" << endl;
			cin >> input;
			db.grade(input);
			cout << endl;
		}
		else if (option == "7") {
			db.displayAll();
			cout << endl;
		}
		else if (option == "8") {
			string tmp = db.findTop(); 
			db.display(tmp);
			cout << endl;
		}
		else if (option == "q") {
			db.close();
			break; //note not using exit due to resource cleanup
		}
		
	} //end event loop

	cout << "\nFinal count:" << endl;
	StudentRecord::print_counts(std::cout,"StudentRecord"); //final count
	
	cout << "\nFinal token check:" << endl;
	tokenlib::final_token_check();
	
	return 0;
}
