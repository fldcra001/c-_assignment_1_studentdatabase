/*
 * StudentRecord.cpp
 * 
 * Class to model a student record
 * 
 * Created on 2 March 2014
 * Craig Feldman
 */
 
 #include "StudentRecord.h"
 #include <sstream> 
 #include <iomanip>

 namespace fldcra001 {


	//destructor
	StudentRecord::~StudentRecord() {
		if (StudentRecord::token_ != -1) {
		  tokenlib::release_token(token_);
		}
		//std::cout << "DESTROYED" << name_ << std::endl;
	}
	
	//move assignment from rhs
	StudentRecord & StudentRecord::operator=(StudentRecord && rhs) {
		using namespace std;
		if (this != &rhs)
		{
			name_ = move(rhs.name_);
			surname_ = move(rhs.surname_);
			studentNumber_ = move(rhs.studentNumber_);
			classRecord_ = move(rhs.classRecord_);
			
			if (token_ != -1) {
				tokenlib::release_token(token_);
			}
				
			token_ = rhs.token_;
			rhs.token_ = -1; //wont attempt to destroy
			
		} 
		
		return *this; // return ref to current obj
	}
	
	//copy assignment
	StudentRecord & StudentRecord::operator=(const StudentRecord & rhs) {
		if (this != &rhs)
		{
			name_ = rhs.name_;
			surname_ = rhs.surname_;
			studentNumber_ = rhs.studentNumber_;
			classRecord_ = rhs.classRecord_;
		}
		
		return *this;
	}
	
	//overload << for Student Record. i.e. when printing
	std::ostream& operator<<(std::ostream& os, const StudentRecord& sr) {
		using std::setw;
		os << sr.name_ << setw(20) << sr.surname_ << setw(25) << sr.studentNumber_ << setw(20) << sr.classRecord_ << std::endl;
		//os << "Student Number:" << sr.studentNumber_ << endl;
		
		return os;
	}
	
	//overload >>
	std::istream& operator>>(std::istream& is, StudentRecord& sr) {
		/*
		is >> StudentRecord::name_;
		is >> surname_;
		is >> studentNumber_;
		
		is >> classRecord_;
		 */
		return is;
	}
	
	//getters
	std::string StudentRecord::getName() {return name_;}
	std::string StudentRecord::getStudentNumber() {return studentNumber_;}
  
  	double StudentRecord::getAverage() {
		double mark, total, i = 0;
		std::stringstream stream(classRecord_);
		while (stream >> mark) {
			total += mark;
			i++;
		}
		
		return total/i;
	}

		
} //namespace

 
