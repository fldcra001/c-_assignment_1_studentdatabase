# make file
# CSC 3022H Assignment 1
# Craig Feldman

CC = g++ 												# compiler	
CUSTOMDIR =												# custom package location		
LIBDIR = -L$(CUSTOMDIR)./tokenlib						# Library location
INCLUDES = -I $(CUSTOMDIR)./tokenlib					# header file locations
LIBS=-l$(CUSTOMDIR)tokenlib	-lboost_program_options		# library (token lib and boost)
LDFLAGS=$(LIBDIR) $(LIBS)								# libs + their locations
CXXFLAGS=$(INCLUDES) -std=c++0x	-Wall					# flags for the c++ compiler; header +all warnings; c++11
TARGET=myprog											# executable name
OBJECTS=driver.o Database.o StudentRecord.o cmdline_parser.o		# object files to build into exe

# Linking Rule
$(TARGET): $(OBJECTS)
	$(CC) $(OBJECTS) -o $(TARGET) $(LDFLAGS) -Wl,-rpath=./tokenlib
#@cp $(TARGET) ./binaries 	#move to binaries

# Macro to associate *.o with *.cpp
.cpp.o:
	$(CC) $(CXXFLAGS) -c $<

clean:
	@rm -f *.o
