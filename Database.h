/*
 * Database.h
 * 
 * Created on 8 March 2014
 * Craig Feldman
 */
 
 #ifndef DATABASE_H
 #define DATABASE_H
 
 #include <string>
 #include <list>
 #include "StudentRecord.h"
 #include <memory>
 
 namespace fldcra001{
	 class Database
	 {
		private:
		std::list<std::unique_ptr<StudentRecord>> records;


		static void printHeader();
		
		public: 
		
		//constructor
		Database(void) {};
	 
		void addStudent(void);
		void remove(std::string studentNum);
		void read(std::string file);
		void save(std::string file);
		void display(std::string studentNum);
		void grade(std::string studentNum);
		void displayAll(void);
		std::string findTop(void);
		void close(void);
		
	}; //class
	 
} //namespace
 
 #endif
